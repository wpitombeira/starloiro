<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Game
        <small><a href="./index.php?m=products">Back</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="./../"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="./index.php?m=products"><i class="fa fa-users"></i> Product List</a></li>
        <li class="active">View Game</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-11 connectedSortable">
            <?php
            if (isset($_GET['id']) && !empty($_GET['id'])) {
              $id = $_GET['id'];
            } else {
              $id = "0";
            }

             $game = DBRead ('products', "WHERE p_id = $id");

            if (!$game) {
              echo "<h2>This Game wasn't found.</h2>";
            } else foreach ($game as $g):
              $p_name = $g['p_name'];
              $p_desc = $g['p_description'];
              $p_value = $g['p_value'];
              $p_storage = $g['p_storage'];
              $p_photo = $g['p_photo'];
              $p_c_id = $g['p_c_id'];
              $p_video = $g['p_video'];

            endforeach;
            ?>
          <!-- quick post widget -->

          <div class="tab-content">
            <div class="tab-pane active">
              <h1 class="text-center"><?=$p_name?></h1>
              <p class="text-center">Description</p>
              <p class="text-center"><small><?=$p_desc?></small></p>
              <div class="text-center">
                <img src="./dist/img/upload/<?=$p_photo?>" class="rounded">
              </div>

              <?php 
              if ($p_video != "svideo") {
                $video_link = explode("=", $p_video);
                $param = count($video_link);
                $link = $video_link[$param-1];

                echo '<div class="embed-responsive embed-responsive-16by9">';
                echo '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' . $link . '" allowfullscreen></iframe>';
                echo "</div>";
              } ?>
          </div>
        </div>
        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="./../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./../dist/js/demo.js"></script>
<!-- CK Editor -->
<script src="./../bower_components/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
</body>
</html>
