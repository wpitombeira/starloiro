<?php
include_once('./system/database.php');
include('./system/restrito.php');

?>
    <section class="content-header">
      <h1>
        Users Management
        <small><a href="./index.php?m=adduser">New User</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>E-Mail</th>
                  <th>Photo</th>
                  <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    $users = DBRead ('users');

                    if (!$users)
                      echo "<h2>0 Users Found</h2>";
                    else
                      foreach ($users as $post):
                        $id = $post['u_id'];
                        $name = $post['u_name'];
                        $email = $post['u_email'];
                        $id = $post['u_id'];
                        $foto = $post['u_photo'];
                  ?>
                  <tr>
                    <td><?=$id?></td>
                    <td><?=$name?></td>
                    <td><?=$email?></td>
                    <td><img style="max-width: 64px;" class="img-circle" src="./dist/img/<?=$foto?>"></td>
                    <td><a href="./index.php?m=edituser&id=<?php echo $post['u_id']; ?>"><span class="glyphicon glyphicon-cog"></span>  <a href="#"><span class="glyphicon glyphicon-trash"></span></td>
                  </tr>
                  <?php
                endforeach;
                ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>E-Mail</th>
                  <th>Photo</th>
                  <th>Manage</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>