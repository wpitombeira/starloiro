<?php
  include_once('./system/database.php');
  include('./system/restrito.php');

if (isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['action']) && !empty($_GET['id'])) {
  $id = DBEscape(strip_tags(trim($_GET['id'])));
    switch ($_GET['action']) {
      case 'print':
        break;
      case 'delete':
        DBDelete('reports', "r_id = '{$id}'");
      break;
    }
}

?>

    <section class="content-header">
      <h1>
        Report List
        <small><a href="./index.php?m=newreport">New Report</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="./../"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reports List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Reports
              <small><a href="./index.php?m=newreport">New Report</a></small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>View</th>
                  <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $author = $_SESSION['UserId'];
                    $posts = DBRead ('reports', 'WHERE r_authorId = "$author" ORDER BY r_date DESC');

                    if (!$posts)
                      echo "<h2>0 reports found!</h2>";
                    else
                      foreach ($posts as $post):
                        $title = $post['r_title'];
                        $id = $post['r_id'];
                        $author = $post['r_authorId'];
                        $date = $post['r_date'];
                        $content = strip_tags($post['r_content']);

                  ?>
                  <tr>
                    <td><?=$id?></td>
                    <td><?=$title?></td>
                    <td><?=$author?></td>
                    <td><a href="./index.php?m=viewreport&id=<?=$id?>">View</a></td>
                    <td><a href="./index.php?m=reports&action=delete&&id=<?php echo $post['r_id'];?>" title="Deletar"><span class="glyphicon glyphicon-trash"></span></a> 
                  </tr>
                  <?php
                endforeach;
                ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>View</th>
                  <th>Manage</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery 3 -->
<script src="./../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="./../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="./../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="./../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>