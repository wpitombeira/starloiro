<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
?>

    <section class="content-header">
      <h1>
        Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="./../dist/img/<?php echo $_SESSION['UsuarioFoto'] ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $_SESSION['UserName'] ?></h3>


              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Member since</b> <a class="pull-right"><?php echo $_SESSION['UserAccessDate'] ?></a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> E-Mail</strong>

              <p class="text-muted">
                <?php echo $_SESSION['UserEmail']; ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Login</strong>

              <p class="text-muted"><?php echo $_SESSION['UserLogin']; ?></p>

              <hr>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#settings" data-toggle="tab">Configuration</a></li>
            </ul>
              <div class="tab-pane" id="settings">
                <form class="form-horizontal"  enctype="multipart/form-data" action="./system/update-user.php" method="post">
                  
                  <input type="hidden" name="u_id" value="<?php $_SESSION['UserId']; ?>">
                  <input type="hidden" name="u_access" value="<?php $_SESSION['UserAccess']; ?>">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="u_name" value="<?php echo $_SESSION['UserName']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">E-Mail</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" name="u_email" value="<?php echo $_SESSION['UsuarioEmail']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputName" name="u_password" value="<?php echo $_SESSION['UserPassword']; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputFoto" class="col-sm-2 control-label">Profile Photo</label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control" id="inputSkills">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Update Profile</button>
                    </div>
                  </div>

                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>

<!-- jQuery 3 -->
<script src="./../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="./../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./../dist/js/demo.js"></script>
