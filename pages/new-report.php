<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
?>

    <section class="content-header">
      <h1>
        New Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">New Report</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-11 connectedSortable">
          <!-- quick post widget -->
          <div class="tab-content">
            <div class="tab-pane active">
              <form class="form-horizontal" method="post" action="./system/new-report.php" enctype="multipart/form-data">
                <?php $userId = $_SESSION['UserId']; ?>
                  <input type="hidden" name="userId" value="$UserId">
                <div class="form-group">
                  <label for="r_category" class="col-sm-2">Category</label>
                  <div class="col-sm-4">
                    <select name="r_category" class="form-control"> 
                      <option value="151099">All</option>
                      <?php
                      $categories = DBRead ('category');

                      if (!$categories) {
                      echo "error";
                    } else foreach ($categories as $cat) {
                    $catId = $cat['c_id'];
                    $catName = $cat['c_name']; ?>
                    <option value="<?=$catId; ?>"><?=$catName?></option>
                <?php  } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="r_product" class="col-sm-2">Produto</label>
                  <div class="col-sm-4">
                  <select class="form-control" name="r_product">
                    <option value="151099">All</option>
                    <?php $products = DBRead ('products');
                    if (!$products) {
                      echo "Error";
                    } else foreach ($products as $prod) {
                      $prodId = $prod['p_id'];
                      $prodName = $prod['p_name'];
                      ?>
                      <option value="<?=$prodId?>"><?=$prodName?></option>
                  <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <div class="box-footer">
                    <input type="submit" name="add" class="btn btn-primary" value="Add">
                  </div>
                </div>
          </div>

        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

      </div>
      <!-- /.box -->

    </section>