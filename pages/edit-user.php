<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit User
        <small><a href="./index.php?m=users">Back</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="./../"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="./users.php"><i class="fa fa-users"></i> User List</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-11 connectedSortable">
            <?php
            if (isset($_GET['id']) && !empty($_GET['id'])) {
              $id = $_GET['id'];
            } else {
              $id = "0";
            }

             $users = DBRead ('users', "WHERE u_id = $id");

            if (!$users) {
              echo "<h2>This user wasn't found.</h2>";
            } else foreach ($users as $user):
              $userName = $user['u_name'];
              $userEmail = $user['u_email'];
              $userPassword = $user['u_password'];
              $userLogin = $user['u_login'];
              $userImage = $user['u_photo'];
              $userAccess = $user['u_access'];
            endforeach;
            ?>
          <!-- quick post widget -->

          <div class="tab-content">
            <div class="tab-pane active">
              <form class="form-horizontal" method="post" action="./system/edit-user.php" enctype="multipart/form-data">
                  <input type="hidden" name="u_login" value="<?php echo $userLogin; ?>">
                <div class="form-group">
                  <label for="u_id" class="col-sm-2 control-label">ID</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" name="u_id2" value="<?php echo $id; ?>" disabled required>
                    <input type="hidden" name="u_id" value="<?php echo $id; ?>">
                  </div>

                  <label for="u_name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="u_name" value="<?php echo $userName; ?>" placeholder="Nome" required>
                  </div> 
                </div>

                <div class="form-group">
                  <label for="u_access" class="col-sm-2 control-label">Access</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="u_access" required>
                      <option value="1" <?php if($userAccess = 1) { echo "selected";}?>>Normal User</option>
                      <option value="2" <?php if($userAccess = 2) { echo "selected";}?>>Super User</option>
                    </select>
                  </div>

                  <label for="u_email" class="col-sm-2 control-label">E-Mail</label>
                  <div class="col-sm-4">
                    <input type="email" class="form-control" name="u_email" placeholder="E-Mail" value="<?php echo $userEmail; ?>">
                  </div>
                </div>
                
                <hr>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Photo:</label>
                  <div class="col-sm-2">
                    <img style="max-width: 100px;" src="./dist/img/<?php echo $userImage; ?>">
                  </div>

                  <label for="u_foto" class="col-sm-2 control-label">Update Photo</label>
                  <div class="col-sm-4">
                    <input type="file" name="u_image" class="form-control-file">
                  </div>
                </div>
                <div class="form-group">
                <div class="box-footer">
                  <input type="submit" name="atualizar" class="btn btn-primary" value="Atualizar">
                </div>
              </div>
            </form>
          </div>
        </div>
        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="./../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./../dist/js/demo.js"></script>
<!-- CK Editor -->
<script src="./../bower_components/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
</body>
</html>
