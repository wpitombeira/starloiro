<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
?>

    <section class="content-header">
      <h1>
        New User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">New User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-11 connectedSortable">
          <!-- quick post widget -->
          <div class="tab-content">
            <div class="tab-pane active">
              <form class="form-horizontal" method="post" action="./../system/add-user.php" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="u_Name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="u_name" placeholder="Name" required>
                  </div> 
                </div>

                <div class="form-group">
                  <label for="u_login" class="col-sm-2 control-label">Login</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" name="u_login" placeholder="Login">
                  </div>

                  <label for="u_password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-2">
                    <input type="password" class="form-control" name="u_password" placeholder="Password">
                  </div>
                </div>

                <div class="form-group">
                  <label for="u_access" class="col-sm-2 control-label">Access</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="u_access" required>
                      <option value="1">Normal User</option>
                      <option value="2">Super User</option>
                    </select>
                  </div>

                  <label for="u_email" class="col-sm-2 control-label">E-Mail</label>
                  <div class="col-sm-4">
                    <input type="email" class="form-control" name="u_email" placeholder="E-Mail" required>
                  </div>
                </div>
                
                <hr>

                <div class="form-group">
                  <label for="u_foto" class="col-sm-2 control-label">Foto</label>
                  <div class="col-sm-4">
                    <input type="file" name="arquivo" class="form-control-file">
                  </div>
                </div>

                <div class="box-footer">
                  <input type="submit" name="add" class="btn btn-primary" value="Add">
                </div>
            </div>
          </div>

        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->