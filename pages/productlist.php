<?php
include_once('./system/database.php');
include('./system/restrito.php');

?>
    <section class="content-header">
      <h1>
        Product List
        <small><a href="./index.php?m=newgame">New Product</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="./"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Product List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Value</th>
                  <th>Storage</th>
                  <th>View</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    $users = DBRead ('products');

                    if (!$users)
                      echo "<h2>0 Users Found</h2>";
                    else
                      foreach ($users as $post):
                        $id = $post['p_id'];
                        $name = $post['p_name'];
                        $p_desc = $post['p_description'];
                        $p_value = $post['p_value'];
                        $p_storage = $post['p_storage'];
                  ?>
                  <tr>
                    <td><?=$id?></td>
                    <td><?=$name?></td>
                    <td><?=$p_desc?></td>
                    <td><?=$p_value?></td>
                    <td><?=$p_storage?></td>
                    <td><a href="./index.php?m=view&id=<?=$id?>">View</a></td>
                  </tr>
                  <?php
                endforeach;
                ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Value</th>
                  <th>Storage</th>
                  <th>View</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>