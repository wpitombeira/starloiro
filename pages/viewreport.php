<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
if (isset($_GET['id'])) {
  $id = $_GET['id'];
} else {
  $id = 0;
}
?>

    <section class="content-header">
      <h1>
        View Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="./../"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> View Report</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Reports
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Value</th>
                  <th>Storage</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    $posts = DBRead ('reports', 'WHERE r_id ='. $id);

                    if (!$posts)
                      echo "<h2>0 reports found!</h2>";
                    else
                      foreach ($posts as $post):
                        $content = $post['r_content'];
                      echo $content;
                endforeach;
                ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Value</th>
                  <th>Storage</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery 3 -->
<script src="./../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="./../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="./../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="./../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>