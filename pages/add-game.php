<?php
  include_once('./system/database.php');
  include('./system/restrito.php');
?>

    <section class="content-header">
      <h1>
        New Game
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">New Game</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-11 connectedSortable">
          <!-- quick post widget -->
          <div class="tab-content">
            <div class="tab-pane active">
              <form class="form-horizontal" method="post" action="./system/add-game.php" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="p_name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="p_name" placeholder="Name" required>
                  </div> 
                </div>

                <div class="form-group">
                  <label for="u_login" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="p_description" placeholder="Description">
                  </div>
                </div>

                <div class="form-group">
                  <label for="p_category" class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="p_c_id" required>
                      <?php
                      $categories = DBRead ('category');

                      if (!$categories) {
                        echo "error";
                      } else foreach ($categories as $cat) {
                      $catId = $cat['c_id'];
                      $catName = $cat['c_name']; ?>
                      ?>
                    <option value="<?=$catId;?>"><?=$catName;?></option>
                    <?php } ?>                      
                    </select>
                  </div>

                  <label for="p_value" class="col-sm-2 control-label">Value</label>
                  <div class="col-sm-2">
                    <input type="number" class="form-control" name="p_value" required>
                  </div>

                  <label for="p_storage" class="col-sm-2 control-label">Storage</label>
                  <div class="col-sm-2">
                    <input type="number" name="p_storage" class="form-control">
                  </div>
                </div>
                
                <hr>

                <div class="form-group">
                  <label for="p_photo" class="col-sm-2 control-label">Photo</label>
                  <div class="col-sm-4">
                    <input type="file" name="arquivo" class="form-control-file">
                  </div>
                </div>
                <div class="form-group">
                  <label for="p_video" class="col-sm-2 control-label">YouTube Video</label>
                  <div class="col-sm-8">
                    <input type="text" name="p_video" class="form-control">
                  </div>
                </div>

                <div class="box-footer">
                  <input type="submit" name="add" class="btn btn-primary" value="Add">
                </div>
              </form>
            </div>
          </div>

        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->