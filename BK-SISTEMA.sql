CREATE DATABASE  IF NOT EXISTS `askov` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `askov`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: askov
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sl_category`
--

DROP TABLE IF EXISTS `sl_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sl_category` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `c_description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sl_category`
--

LOCK TABLES `sl_category` WRITE;
/*!40000 ALTER TABLE `sl_category` DISABLE KEYS */;
INSERT INTO `sl_category` VALUES (1,'RPG','Role-playing game, também conhecido como RPG (em português: \"jogo de interpretação de papéis\" ou \"jogo de representação\"), é um tipo de jogo em que os jogadores assumem papéis de personagens e criam narrativas colaborativamente. O progresso de um jogo se dá de acordo com um sistema de regras predeterminado, dentro das quais os jogadores podem improvisar livremente. As escolhas dos jogadores determinam a direção que o jogo irá tomar.'),(2,'Aventura','São jogos em que o jogador assume o papel de um protagonista em uma história interativa com exploração e resolução de quebra-cabeças.'),(3,'VR','Jogos de realidade virtual, tentam ao maximo se igualar a realidade.'),(4,'Corrida','Jogos que simulam disputas de carros.');
/*!40000 ALTER TABLE `sl_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sl_products`
--

DROP TABLE IF EXISTS `sl_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sl_products` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `p_description` text COLLATE utf8_unicode_ci NOT NULL,
  `p_c_id` int(11) NOT NULL,
  `p_value` decimal(10,2) NOT NULL,
  `p_storage` int(11) NOT NULL DEFAULT '0',
  `p_photo` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NoGamePhoto.jpg',
  `p_video` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'svideo',
  PRIMARY KEY (`p_id`),
  UNIQUE KEY `g_name_UNIQUE` (`p_name`),
  KEY `category_id_game_idx` (`p_c_id`),
  CONSTRAINT `productxcategoryforeing` FOREIGN KEY (`p_c_id`) REFERENCES `sl_category` (`c_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sl_products`
--

LOCK TABLES `sl_products` WRITE;
/*!40000 ALTER TABLE `sl_products` DISABLE KEYS */;
INSERT INTO `sl_products` VALUES (1,'Final Fantasy XIV: Stormblood','Seguindo o sucesso de Heavensward, a segunda expansão de Final Fantasy XIV, que levou o nome de Stormblood, fez jus à sua colocação como uma das maiores franquias de RPG da atualidade. O jogo foi considerado um dos melhores jogos de PC do ano e ganhou muitos elogios pela crítica.',1,199.90,300,'NoGamePhoto.jpg','https://www.youtube.com/watch?v=Jt1h1MinlLI'),(2,'Night In The Woods','A história segue Mae Borowski, uma gata de 20 anos que decide largar a universidade e voltar para a casa dos pais. Tentando fugir dos questionamentos que surgem com essa volta e procurando reatar laços perdidos pelo tempo e distância, o game prende a atenção do jogador e estimula a afeição pelos personagens da trama.',2,50.00,100,'NoGamePhoto.jpg','svideo'),(3,'Cuphead','O game traz cenários bizarros e inimigos poderosos, que causam euforia quando finalmente são derrotados. Cupheads tem uma jogabilidade e um estilo que lembra antigos jogos de Super Nintendo e Megadrive, como MegaMan e Castlevania, em que observar seu oponente faz toda a diferença.',2,100.00,20,'NoGamePhoto.jpg','svideo'),(4,'XCOM 2: War of the Chosen','A história segue um curso parecido com o da versão original, mas com boas adições. Uma delas é o trio de aliens chamados Chosen, que aumentam significantemente o desafio do jogo e atuam em momentos específicos da história.',2,299.00,500,'NoGamePhoto.jpg','svideo'),(5,'What Remains of Edith Finch','Ao explorar sua casa, Edith conta a história de seus parentes, todos já falecidos. Existe uma possível maldição na família, que faz com que todos morram jovens, e é torno desta temática e da busca de Edith por respostas que o jogo segue. Com um tom literário e riqueza em detalhes, esse game é ideal para quem gosta de desvendar mistérios.',1,20.00,1000,'NoGamePhoto.jpg','svideo'),(6,'Rez Infinite','Com um tom um tanto quanto psicodélico, Rez Infinite explora as tecnologias recentes como realidade virtual, mas sem cair na armadilha de reduzir o game a apenas uma experiência tecnológica. O jogo já contava com uma versão para PlayStation 4, com suporte ao PSVR, mas muitos consideram a edição para PC como a versão definitiva dessa experiência',3,199.00,50,'NoGamePhoto.jpg','svideo'),(7,'F1 2017','Se o F1 2016 já foi um jogo de deixar amantes de jogos de corrida de boca aberta, o F1 2017 conseguiu superar seu antecessor. Além de trazer todos os pontos fortes do game de 2016, inclusive reciclando diversas animações e linhas de diálogo, F1 2017 volta com as corridas com carros clássicos. São doze modelos clássicos disponíveis nesta versão. O usuário pode encontrar, inclusive, as McLarens utilizadas por Ayrton Senna em 88 e 91.',4,300.00,60,'NoGamePhoto.jpg','svideo'),(8,'Lone Echo','O protagonista do jogo é Jack, o androide responsável pela operação de Kronos, uma estação espacial mineradora que orbita Saturno. O universo criado é detalhado e o esforço dos desenvolvedores foi perceptível. Lone Echo pode ser qualificado como um dos melhores jogos para realidade virtual já lançados.',3,100.00,58,'NoGamePhoto.jpg','svideo'),(9,'Bayonetta','Lançado em 2009, originalmente para Xbox 360 e Playstation 3, Bayonetta finalmente chega ao PC. Quem curte jogar no PC sabe que ports de clássicos para a plataforma são arriscados.  Infelizmente, ports relaxados ou decepcionantes não são raros. Porém, para a felicidade dos fãs do gênero hack and slash, o game mostra como um port de qualidade deve ser feito',2,500.00,5,'NoGamePhoto.jpg','svideo'),(10,'Divinity: Original Sin II','Com mecânicas e gameplay mais refinado e fluído que seu antecessor, o universo do jogo foi construído com primor em todos os detalhes. Por mais que a jogabilidade não seja nenhuma novidade, seguindo o padrão de RPGs e batalhas por turnos, a inteligência artificial impressiona com a capacidade de ficar mais refinada conforme você evolui seu personagem.',1,200.00,60,'NoGamePhoto.jpg','svideo');
/*!40000 ALTER TABLE `sl_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sl_reports`
--

DROP TABLE IF EXISTS `sl_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sl_reports` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `r_title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `r_date` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `r_content` longtext CHARACTER SET latin1,
  `r_authorId` int(11) NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sl_reports`
--

LOCK TABLES `sl_reports` WRITE;
/*!40000 ALTER TABLE `sl_reports` DISABLE KEYS */;
INSERT INTO `sl_reports` VALUES (37,'1','15-24-2018-31-03','<tr><td>1</td><td>Final Fantasy XIV: Stormblood</td><td>Seguindo o sucesso de Heavensward, a segunda expansão de Final Fantasy XIV, que levou o nome de Stormblood, fez jus à sua colocação como uma das maiores franquias de RPG da atualidade. O jogo foi considerado um dos melhores jogos de PC do ano e ganhou muitos elogios pela crítica.</td><td>199.90</td><td>300</td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr>',0),(38,'1','15-24-2018-32-36','<tr><td>1</td><td>Final Fantasy XIV: Stormblood</td><td>199.90</td><td>300</td></tr><tr><td></td><td></td><td></td><td></td></tr>',0),(39,'151099','15-24-2018-33-00','<tr><td>1</td><td>Final Fantasy XIV: Stormblood</td><td>199.90</td><td>300</td></tr><tr><td></td><td></td><td></td><td></td></tr>',0),(40,'151099','15-24-2018-33-21','<tr><td></td><td></td><td></td><td></td></tr>',0),(41,'1','15-24-2018-54-59','<tr><td>1</td><td>Final Fantasy XIV: Stormblood</td><td>199.90</td><td>300</td></tr><tr><td></td><td></td><td></td><td></td></tr>',0),(42,'1','15-24-2018-55-06','<tr><td>1</td><td>Final Fantasy XIV: Stormblood</td><td>199.90</td><td>300</td></tr><tr><td></td><td></td><td></td><td></td></tr>',0),(43,'151099','18-25-2018-01-37','<tr><td>1</td><td>Final Fantasy XIV: Stormblood</td><td>199.90</td><td>300</td></tr><tr><td>2</td><td>Night In The Woods</td><td>50.00</td><td>100</td></tr><tr><td>3</td><td>Cuphead</td><td>100.00</td><td>20</td></tr><tr><td>4</td><td>XCOM 2: War of the Chosen</td><td>299.00</td><td>500</td></tr><tr><td>5</td><td>What Remains of Edith Finch</td><td>20.00</td><td>1000</td></tr><tr><td>6</td><td>Rez Infinite</td><td>199.00</td><td>50</td></tr><tr><td>7</td><td>F1 2017</td><td>300.00</td><td>60</td></tr><tr><td>8</td><td>Lone Echo</td><td>100.00</td><td>58</td></tr><tr><td>9</td><td>Bayonetta</td><td>500.00</td><td>5</td></tr><tr><td>10</td><td>Divinity: Original Sin II</td><td>200.00</td><td>60</td></tr><tr><td></td><td></td><td></td><td></td></tr>',0);
/*!40000 ALTER TABLE `sl_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sl_users`
--

DROP TABLE IF EXISTS `sl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sl_users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `u_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `u_password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `u_email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `u_birthdate` date DEFAULT NULL,
  `u_access` int(11) NOT NULL DEFAULT '1',
  `u_photo` varchar(60) COLLATE utf8_unicode_ci DEFAULT 'NoPhoto.jpg',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_email_UNIQUE` (`u_email`),
  UNIQUE KEY `u_login_UNIQUE` (`u_login`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sl_users`
--

LOCK TABLES `sl_users` WRITE;
/*!40000 ALTER TABLE `sl_users` DISABLE KEYS */;
INSERT INTO `sl_users` VALUES (1,'Tester','teste','123','teste@teste.com','2018-06-15',2,'avatar5.png'),(2,'walison_thimus@hotmail.com','dasdasd','asdsad','dasdsa@dasdasd.com',NULL,1,'NoPhoto.jpg'),(3,'teste2','teste2','123','fasdf@FASFASDF.com',NULL,1,'NoPhoto.jpg'),(7,'teste23','teste23','123','fadasdsdf@FASFASDF.com',NULL,1,'NoPhoto.jpg');
/*!40000 ALTER TABLE `sl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'askov'
--

--
-- Dumping routines for database 'askov'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-18 10:09:42
