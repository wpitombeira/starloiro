<?php
if (!isset($_SESSION)) session_start();

if (!isset($_SESSION['UserId'])) {
	session_destroy();
	header("Location: ./login.html"); exit;
}
?>