<?php

include('database.php');

if (!empty($_POST) AND (empty($_POST['user']) OR empty($_POST['password']))){
	header("location: ./../index.html"); exit;
}

 $user = mysqli_real_escape_string($conn, $_POST['user']);
 $password = mysqli_real_escape_string($conn, $_POST['password']);


$SQL = "SELECT * FROM sl_users WHERE u_login LIKE '$user' AND u_password = $password LIMIT 1";

$query = mysqli_query($conn,$SQL);

if (mysqli_num_rows($query) != 1) {

	echo "User not found!"; exit;
} else {
	$resultado = mysqli_fetch_assoc($query);

	if (!isset($_SESSION)) session_start();

	$_SESSION['UserId'] = $resultado['u_id'];
	$_SESSION['UserName'] = $resultado['u_name'];
	$_SESSION['UserLogin'] = $resultado['u_login'];
	$_SESSION['UserEmail'] = $resultado['u_email'];
	$_SESSION['UserAccess'] = $resultado['u_access'];
	$_SESSION['UserPassword'] = $resultado['u_password'];
	$_SESSION['UserImage'] = $resultado['u_photo'];
	$_SESSION['UserBirthDate'] = $resultado['u_birthdate'];


	header("Location: ./../");
}